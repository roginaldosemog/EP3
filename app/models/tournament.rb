class Tournament < ApplicationRecord
  mount_uploader :picture, PictureUploader
  belongs_to :user

  def self.search(search)
  	if search
  		where(["name LIKE ?","%#{search}%"])
  	else 
  		all
  	end
  end
end
