json.extract! tournament, :id, :name, :modality, :description, :picture, :created_at, :updated_at
json.url tournament_url(tournament, format: :json)
