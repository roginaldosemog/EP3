class CreateTournaments < ActiveRecord::Migration[5.1]
  def change
    create_table :tournaments do |t|
      t.string :name
      t.string :modality
      t.text :description
      t.string :picture
      t.integer :user_id

      t.timestamps
    end
  end
end
