Rails.application.routes.draw do
  root :to => redirect('/tournaments')

  #get 'home/index'

  resources :tournaments
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
