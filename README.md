# eChamps!

O eChamps é uma aplicação web básica e simples, prática e intuitiva
Tem como funcionalidade permitir que o usuário gerencie e acompanhe torneios esportivos, de várias modalidades, abrangendo a totalidade dos esportes que possuem campeonatos oficiais e não oficiais, profissionais ou recreativos (amadores). 
É possível simplesmente acompanhar os campeonatos de seu interesse, como um visitante, não sendo necessário a criação de um usuário. 
Também é possível, com a criação de um usuário, cadastrar um novo campeonato e ser o gerente (responsável por sua atualização). 
O usuário administrador tem permissões para criar, editar e deletar sobre todos os campeonatos.
O eChamps também implementa uma busca sobre o nome dos torneios. Se o campo de tal busca estiver em branco, ela retorna todos os torneios cadastrados.
O objetivo desta aplicação é proporcionar uma maior divulgação e participação dos campeonatos esportivos. As comunidades, tanto de e-sports quanto de esportes tradicionais, se beneficiaram com tal aplicação. A divulgação e estímulo à participação de campeonatos não oficiais também movimentam as torcidas, o interesse e o suporte à modalidade (esporte ou e-sports).

INFORMAÇÕES:

* Ruby 2.4

* Rails 5.1

* To test

    bundle install
    
    rake db:migrate
    
    rails server
    
- On the browser:

    localhost:3000/